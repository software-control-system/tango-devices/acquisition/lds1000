static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Instrumentation/LDS1000/src/LDS1000.cpp,v 1.4 2010-03-26 09:30:11 vince_soleil Exp $";
//+=============================================================================
//
// file :         LDS1000.cpp
//
// description :  C++ source for the LDS1000 and its commands. 
//                The class is derived from Device. It represents the
//                CORBA servant object which will be accessed from the
//                network. All commands which can be executed on the
//                LDS1000 are implemented in this file.
//
// project :      TANGO Device Server
//
// $Author: vince_soleil $
//
// $Revision: 1.4 $
//
// $Log: not supported by cvs2svn $
// Revision 1.3  2008/08/08 12:30:05  julien_malik
// compil VC8
//
// Revision 1.2  2006/03/28 07:29:18  sebleport
// Changes in dev_state and creation of the integration time attribute.
//
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================



//===================================================================
//
//	The folowing table gives the correspondance
//	between commands and method's name.
//
//  Command's name|  Method's name
//	----------------------------------------
//  State   |  dev_state()
//  Status  |  dev_status()
//  Start   |  start()
//  Reset   |  reset()
//  Stop    |  stop()
//
//===================================================================


#include <tango.h>
#include <LDS1000.h>
#include <LDS1000Class.h>

#include "PogoHelper.h"
#include "Xstring.h"
#include <math.h>

namespace LDS1000_ns
{
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::LDS1000(string &s)
  // 
  // description : 	constructor for simulated LDS1000
  //
  // in : - cl : Pointer to the DeviceClass object
  //      - s : Device name 
  //
  //-----------------------------------------------------------------------------
  LDS1000::LDS1000(Tango::DeviceClass *cl,string &s)
    :Tango::Device_4Impl(cl,s.c_str())
  {
    init_device();
  }
  
  LDS1000::LDS1000(Tango::DeviceClass *cl,const char *s)
    :Tango::Device_4Impl(cl,s)
  {
    init_device();
  }
  
  LDS1000::LDS1000(Tango::DeviceClass *cl,const char *s,const char *d)
    :Tango::Device_4Impl(cl,s,d)
  {
    init_device();
  }
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::delete_device()
  // 
  // description : 	will be called at device destruction or at init command.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::delete_device()
  {
    //	Delete device's allocated object
    
    // before leaving, set the device in local mode 
    std::string response = write_read("ml",false);
    
    if (measurements_y)
      delete [] measurements_y;
    measurements_y=0;
    
    if (measurements_z)
      delete [] measurements_z;
    measurements_z=0;
    
    if(mySerial)
    {
      delete mySerial;
      mySerial = 0;
    }
    
    start_triggered=false;
    
    DELETE_SCALAR_ATTRIBUTE(attr_integrationConstant_read);
    DELETE_SCALAR_ATTRIBUTE(attr_angleY_read);
    DELETE_SCALAR_ATTRIBUTE(attr_angleZ_read);
    DELETE_SCALAR_ATTRIBUTE(attr_countingCompleted_read);
    DELETE_SCALAR_ATTRIBUTE(attr_angleYStdDeviation_read);
    DELETE_SCALAR_ATTRIBUTE(attr_angleZStdDeviation_read);
    DELETE_SCALAR_ATTRIBUTE(attr_numberSamples_read);
    DELETE_SCALAR_ATTRIBUTE(attr_samplePeriod_read);
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::init_device()
  // 
  // description : 	will be called at device initialization.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::init_device()
  {
    INFO_STREAM << "LDS1000::LDS1000() create device " << device_name << endl;
    
    // Initialise variables to default values
    //--------------------------------------------
    get_device_property();
    
    short val = 1;
    
    CREATE_SCALAR_ATTRIBUTE(attr_integrationConstant_read);
    CREATE_SCALAR_ATTRIBUTE(attr_angleY_read);
    CREATE_SCALAR_ATTRIBUTE(attr_angleZ_read);
    CREATE_SCALAR_ATTRIBUTE(attr_countingCompleted_read,val);
    CREATE_SCALAR_ATTRIBUTE(attr_angleYStdDeviation_read);
    CREATE_SCALAR_ATTRIBUTE(attr_angleZStdDeviation_read);
    CREATE_SCALAR_ATTRIBUTE(attr_numberSamples_read);
    CREATE_SCALAR_ATTRIBUTE(attr_samplePeriod_read);
    
    //pointer to device proxy initialized to 0
    mySerial = 0;
    
    init_LDS1000_done = false;
    reset_LDS1000 = false;
    reading_finished = true;
    
    attr_numberSamples_write = 0;
    measurements_y=0;
    measurements_z=0; 
    
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::readDeviceProperies()
  // 
  // description : 	Read the device properties from database.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::get_device_property()
  {
    //	Initialize your default values here.
    //------------------------------------------
    
    
    //	Read device properties from database.(Automatic code generation)
    //-------------------------------------------------------------
	if (Tango::Util::instance()->_UseDb==false)
		return;
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("SerialProxyName"));

	//	Call database and extract values
	//--------------------------------------------
	get_db_device()->get_property(dev_prop);
	LDS1000Class	*ds_class =
		(static_cast<LDS1000Class *>(get_device_class()));
	int	i = -1;
	//	Extract SerialProxyName value
	if (dev_prop[++i].is_empty()==false)	dev_prop[i]  >>  serialProxyName;
	else
	{
		//	Try to get value from class property
		Tango::DbDatum	cl_prop = ds_class->get_class_property(dev_prop[i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  serialProxyName;
	}



    //	End of Automatic code generation
    //-------------------------------------------------------------
    
  }
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::always_executed_hook()
  // 
  // description : 	method always executed before any command is executed
  //
  //-----------------------------------------------------------------------------
  void LDS1000::always_executed_hook()
  {
    dev_state();
    
  }
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_attr_hardware
  // 
  // description : 	Hardware acquisition for attributes.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_attr_hardware(vector<long> &attr_list)
  {
    DEBUG_STREAM << "LDS1000::read_attr_hardware(vector<long> &attr_list) entering... "<< endl;
    //	Add your own code here
    
  }
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_integrationTime
  // 
  // description : 	Extract real attribute values for integrationTime acquisition result.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_integrationTime(Tango::Attribute &attr)
  {
    DEBUG_STREAM << "LDS1000::read_integrationTime(Tango::Attribute &attr) entering... "<< endl;
   
    // The integrationTime attributre is used by the Scan Device. there is no implementation to do here.
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::write_integrationTime
  // 
  // description : 	Write integrationTime attribute values to hardware.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::write_integrationTime(Tango::WAttribute &attr)
  {
    DEBUG_STREAM << "LDS1000::write_integrationTime(Tango::WAttribute &attr) entering... "<< endl;

    // The integrationTime attributre is used by the Scan Device. there is no implementation to do here.
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_integrationConstant
  // 
  // description : 	Extract real attribute values for integrationTime acquisition result.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_integrationConstant(Tango::Attribute &attr)
  {
    DEBUG_STREAM << "LDS1000::read_integrationConstant(Tango::Attribute &attr) entering... "<< endl;
    
    // reading of the integration time parameter
    std::string response = write_read("xi");
    
    // get the value without the header XI
    int length = response.size() - 2;
    
    response = response.substr(2,length);
    
    *attr_integrationConstant_read = XString<double>::convertFromString(response);
    
    attr.set_value(attr_integrationConstant_read); 
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::write_integrationConstant
  // 
  // description : 	Write integrationTime attribute values to hardware.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::write_integrationConstant(Tango::WAttribute &attr)
  {
    DEBUG_STREAM << "LDS1000::write_integrationConstant(Tango::WAttribute &attr) entering... "<< endl;
    
    //get the preset from attr and set the attr_integrationConstant_write
    attr.get_write_value(attr_integrationConstant_write);
    
    //because the preset must be a integer value
    int cons =(int)attr_integrationConstant_write;
    
    // convert the preset type : int to string
    std::string preset = XString<int>::convertToString(cons);
    
    std::string response = write_read("ki" + preset,false); 
    
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_angleY
  // 
  // description : 	Extract real attribute values for angleY acquisition result.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_angleY(Tango::Attribute &attr)
  {
    DEBUG_STREAM << "LDS1000::read_angleY(Tango::Attribute &attr) entering... "<< endl;
    
    process();
    
    attr.set_value(attr_angleY_read);
    
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_angleZ
  // 
  // description : 	Extract real attribute values for angleZ acquisition result.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_angleZ(Tango::Attribute &attr)
  {
    DEBUG_STREAM << "LDS1000::read_angleZ(Tango::Attribute &attr) entering... "<< endl;
    
    process();
    
    attr.set_value(attr_angleZ_read);
    
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_countingCompleted
  // 
  // description : 	Extract real attribute values for countingCompleted acquisition result.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_countingCompleted(Tango::Attribute &attr)
  {
    DEBUG_STREAM << "LDS1000::read_countingCompleted(Tango::Attribute &attr) entering... "<< endl;
    
    // value of acquisition is read in dev_state
    attr.set_value(attr_countingCompleted_read);  
    
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_angleYStdDeviation
  // 
  // description : 	Extract real attribute values for angleYStdDeviation acquisition result.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_angleYStdDeviation(Tango::Attribute &attr)
  {
    DEBUG_STREAM << "LDS1000::read_angleYStdDeviation(Tango::Attribute &attr) entering... "<< endl;
    
    process();
    
    attr.set_value(attr_angleYStdDeviation_read);
    
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_angleZStdDeviation
  // 
  // description : 	Extract real attribute values for angleZStdDeviation acquisition result.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_angleZStdDeviation(Tango::Attribute &attr)
  {
    DEBUG_STREAM << "LDS1000::read_angleZStdDeviation(Tango::Attribute &attr) entering... "<< endl;
    
    process();
    
    attr.set_value(attr_angleZStdDeviation_read);
    
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_numberSamples
  // 
  // description : 	Extract real attribute values for numberSamples acquisition result.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_numberSamples(Tango::Attribute &attr)
  {
    DEBUG_STREAM << "LDS1000::read_numberSamples(Tango::Attribute &attr) entering... "<< endl;
    
    *attr_numberSamples_read=attr_numberSamples_write;
    attr.set_value(attr_numberSamples_read);
    
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::write_numberSamples
  // 
  // description : 	Write numberSamples attribute values to hardware.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::write_numberSamples(Tango::WAttribute &attr)
  {
    DEBUG_STREAM << "LDS1000::write_numberSamples(Tango::WAttribute &attr) entering... "<< endl;
    
    //get the preset from attr and set the attr_name_write
    
    attr.get_write_value(attr_numberSamples_write);
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::read_samplePeriod
  // 
  // description : 	Extract real attribute values for samplePeriod acquisition result.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::read_samplePeriod(Tango::Attribute &attr)
  {
    DEBUG_STREAM << "LDS1000::read_samplePeriod(Tango::Attribute &attr) entering... "<< endl;
    
    std::string response;
    
    int length;
    
    response = write_read("xs");
    
    length = response.size() - 2;
    
    response = response.substr(2,length);
    
    *attr_samplePeriod_read = XString<double>::convertFromString(response);
    
    attr.set_value(attr_samplePeriod_read);
    
  }
  
  //+----------------------------------------------------------------------------
  //
  // method : 		LDS1000::write_samplePeriod
  // 
  // description : 	Write samplePeriod attribute values to hardware.
  //
  //-----------------------------------------------------------------------------
  void LDS1000::write_samplePeriod(Tango::WAttribute &attr)
  {
    DEBUG_STREAM << "LDS1000::write_samplePeriod(Tango::WAttribute &attr) entering... "<< endl;
    
    std::string response;
    
    //get the consign from attr and set the attr_name_write
    attr.get_write_value(attr_samplePeriod_write);
    
    // cast to long beacause the preset must be an integer value
    long sp =(long)attr_samplePeriod_write;
    
    std::string preset = XString<long>::convertToString(sp);
    
    response = write_read("sp" + preset,false);
    
  }
  
  
  //+------------------------------------------------------------------
/**
 *	method:	LDS1000::start
 *
 *	description:	method to execute "Start"
 *	command which triggers the acquisition
 *
 *
 */
//+------------------------------------------------------------------
  void LDS1000::start()
  {
    DEBUG_STREAM << "LDS1000::start(): entering... !" << endl;
    
    //	Add your own code to control device here
    long N;
    
    N = *attr_numberSamples_read;
    
    firstime=true;
    start_triggered=true;
    reading_finished=false;
    
    // delete previous measurements
    if (measurements_y)
      delete [] measurements_y;
    measurements_y=0;
    if (measurements_z)
      measurements_z=0;
    delete [] measurements_z;
    
    // before sending command "TMx" the numebr of samples has to be >
    
    if(N)
    {
      // create an array of Y angle measurements
      measurements_y = new double[N]; 
      // create an array of Z angle measurements
      measurements_z = new double[N];
      
      // convert the preset type long to string,
      std::string n = XString<long>::convertToString(*attr_numberSamples_read);
      
      // start acquisitions
      std::string response = write_read("tm" + n,false);
      
    }//endif
    
  }
  
  //+------------------------------------------------------------------
/**
 *	method:	LDS1000::reset
 *
 *	description:	method to execute "Reset"
 *	reset the device
 *
 *
 */
//+------------------------------------------------------------------
  void LDS1000::reset()
  {
    DEBUG_STREAM << "LDS1000::reset(): entering... !" << endl;
    
    //	Add your own code to control device here
    
    start_triggered=false;
    std::string response = write_read("rs", false);
    reset_LDS1000 = true;
    
  }
  
  //+------------------------------------------------------------------
/**
 *	method:	LDS1000::stop
 *
 *	description:	method to execute "Stop"
 *	stops acquisitions
 *
 *
 */
//+------------------------------------------------------------------
  void LDS1000::stop()
  {
    DEBUG_STREAM << "LDS1000::stop(): entering... !" << endl;
    
    //	Add your own code to control device here
    
    std::string response = write_read("st",false);
    start_triggered=false;
    reading_finished=true;
    
    
  }
  
  //+------------------------------------------------------------------
  /**
  *	method:	LDS1000::dev_state
  *
  *	description:	method to execute "State"
  *	This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
  *
  * @return	State Code
  *
  */
  //+------------------------------------------------------------------
  Tango::DevState LDS1000::dev_state()
  {
    Tango::DevState	argout = DeviceImpl::dev_state();
    DEBUG_STREAM << "LDS1000::dev_state(): entering... !" << endl;
    
    //	Add your own code to control device here
    
    // reading of the error code
    std::string response = write_read("te");
    int length = response.size() - 2;
    response = response.substr(2,length);
    
    char error_code = XString<char>::convertFromString(response);
    
    if(error_code == '@')
    {
      //NO ERROR
      // read acquisition status
      response = write_read("ms");
      int length = response.size() - 2;
      response = response.substr(2,length);
      
      // if bit3 = 0, status = standbye, else status = acquisition
      char status_acquisition_byte = XString<char>::convertFromString(response);
      
      if(status_acquisition_byte & 0x08 || (!reading_finished) )
        
      {
        set_state(Tango::RUNNING);
        set_status("LDS1000 is acquiring");
        *attr_countingCompleted_read = 0;
      }
      else
      {
        set_state(Tango::ON);
        set_status("LDS1000 is waiting for starting acquisitions");
        *attr_countingCompleted_read = 1;
        
      }
      
    }
    else
      
    {
      // ERROR detected, so states updated
      response = write_read("tb"+response);
      int length = response.size() - 3;
      response = response.substr(3,length);
      
      set_state(Tango::FAULT);
      set_status(response);
      
    }
    
    if (reset_LDS1000)
    {
      set_state(Tango::OFF);
      set_status("you have to push the command init ON");
    }
    
    return argout;
  }
  
  
  /******************************************************************************
  
   INTERNAL METHODS
   
  *******************************************************************************/
  // **************************************************************************
  //						write_read
  //
  //		send specific command to the LDS1000 and return its response
  //		argin : string command
  //    argin : read_required is set to true if LDS1000 answers to the command and method should wait for an answer
  //		argout : string response
  //
  // **************************************************************************
  std::string LDS1000::write_read(std::string cmd_to_send,bool read_required)
  {    
    Tango::ConstDevString response="";
    long nb_char_written;       
    
    DEBUG_STREAM << "LDS1000::write_read(): entering... cmd_to_send =" << cmd_to_send << endl;
    
    // Construct a dedicated DevVarCharArray with validation char <CR>
    Tango::DevVarCharArray *CR = new Tango::DevVarCharArray();
    CR->length(1);
    
    (*CR)[0] = 13;
    
    // create a serialline device proxy if it don't exist
    DEBUG_STREAM << "LDS1000::write_read(): entering... init_proxy_Serialline " <<  endl;
    init_proxy_Serialline();
    
    // init LDS1000 if not already done 
    DEBUG_STREAM << "LDS1000::write_read(): entering... init_LDS1000 " <<  endl;
    init_LDS1000();
    
    
    //- init proxy done so :
    try
    {
      //- send command
      mySerial->command_inout("DevSerWriteString", cmd_to_send.c_str(), nb_char_written);
      DEBUG_STREAM << "LDS1000::write_read(): DevSerWriteString called, nb_char_written = " << nb_char_written <<  endl;
      
      mySerial->command_inout("DevSerWriteChar",CR, nb_char_written);
      DEBUG_STREAM << "LDS1000::write_read(): DevSerWriteChar called, nb_char_written = " << nb_char_written <<  endl;
      
      //- read LDS1000 response if required 
      if (read_required ==true)
      {
        long readMode = 2;  //- SL_LINE
        mySerial->command_inout("DevSerReadString", readMode, response);
        DEBUG_STREAM << "LDS1000::write_read(): DevSerReadString called, response" <<response <<  endl;   
        return string(response);
      }
      else
      {
        DEBUG_STREAM << "LDS1000::write_read(): ended" <<   endl;   
        return string(response);       
      }
    }
    catch(Tango::DevFailed &df)
    {
      ERROR_STREAM << df << std::endl;
      
      set_state(Tango::UNKNOWN);
      set_status("No communication with Serial line device");
      delete mySerial;
      mySerial=0;
      
      Tango::Except::re_throw_exception(df,
        (const char*) "CONNECTION_ERROR",
        (const char*) "Cannot send or read data on the SerialLine device ",
        (const char*) "LDS1000::write_read()");
    }
    
  }
  
  // **************************************************************************
  //						process()
  //
  //    description: read n Y and Z angle values from the hardware, then put them 		
  //    each one into an array and finally calculates and updates the average 
  //    and the mean standard deviation.
  //		
  //    argin : void
  //		argout : void
  //
  // **************************************************************************
  void LDS1000::process(void)
  {
    std::string response;
    int length,indexY,indexZ,i,first_point;
    double angle_Y,angle_Z;
    
    // the number of samples to write must be !=0 and start command has to be triggered
    if(attr_numberSamples_write && start_triggered)
      
    {
      // this is firtstime that the programme get in the reading loop, the first point to acquire has got the index 1  
      if(firstime==true)
      {
        first_point = 1;
        firstime = false;
        last_point=0;
      }
      // gets the index of the last point read 
      else
      {
        first_point = last_point + 1;
      }
      
      response = write_read("xn");
      
      length = response.size() - 2;
      
      response = response.substr(2,length);
      
      last_point = XString<int>::convertFromString(response);
      
      // process : reads data, puts data in double arrays and calculates the average and the mean standard deviation for each array
      for(i=first_point;i<=last_point;i++)
        
      { 
        std::string num = XString<int>::convertToString(i);
        
        response = write_read(num + "tt");
        
        //  Gets y and z angles from string response 
        length = response.size();
        
        // looks for the ',' character which separate Y and Z measurements
        indexY = response.find(',',0);
        
        indexZ = response.find(',',indexY+1);
        
        if (indexY != -1 && indexZ != -1)
        {
          angle_Y	 = XString<double>::convertFromString(response.substr(indexY+4,indexZ-indexY-4));
          angle_Z  = XString<double>::convertFromString(response.substr(indexZ+4,length-indexZ-4));
          
          measurements_y[i-1] = angle_Y;
          measurements_z[i-1] = angle_Z;  
        }
        
        else
        {
          // Could not extract values from return strings
          measurements_y[i-1] = 0;
          measurements_z[i-1] = 0;
        }
        
        //cout<<"mes Y:  "<<i<<"  "<<measurements_y[i-1]<<endl;
        //cout<<"mes Z:  "<<i<<"  "<<measurements_z[i-1]<<endl;
        
      }//end for
      
      // Y and Z angle average/mean_std_deviation are calculated once all samples are read
      if(i == attr_numberSamples_write + 1)
      {
        
        // attribute updating
        *attr_angleY_read = average(measurements_y);
        *attr_angleZ_read = average(measurements_z);
        *attr_angleYStdDeviation_read = mean_std_deviation(measurements_y);
        *attr_angleZStdDeviation_read = mean_std_deviation(measurements_z);
        
        start_triggered=false;
        
        reading_finished=true;
        
      }//end if
      
    }//end if
    
  }//end function
  
  // **************************************************************************
  //						init_proxy
  //
  //		allocates memory for a server proxy
  //		argin : string command
  //		argout : string response
  //
  //
  // **************************************************************************
  void LDS1000::init_proxy_Serialline()
  {
    // - Create proxy on Serial if not exits !!! 
    if( !mySerial )
    {
      try
      {
        //- Creates the Proxy
        mySerial =	new Tango::DeviceProxyHelper(serialProxyName,this);
      }
      
      catch(Tango::DevFailed &df)
      {
        ERROR_STREAM << df << std::endl;
        
        set_state(Tango::UNKNOWN);
        set_status("No communication with Serial line device");
        delete mySerial;
        mySerial=0;
        
        Tango::Except::re_throw_exception(df,
          (const char*) "CONNECTION_ERROR",
          (const char*) "Cannot send or read data on the SerialLine device ",
          (const char*) "DS1000::write_read()");
      }
      catch(std::bad_alloc)
      {
        //this->init_device_done = false;
        FATAL_STREAM << "NHQ_x0Xxx::read() : OUT_OF_MEMORY unable to create proxy on SerialLine " << endl;
        set_state(Tango::FAULT);
        set_status("internal error 3 at initialisation ");
        delete mySerial;
        mySerial=0;
      }
      
    } //end if
    
  }
  
  // **************************************************************************
  //						init_LDS1000
  //
  //		
  //		argin : string command
  //		argout : string response
  //
  //
  // **************************************************************************
  void LDS1000::init_LDS1000()
  {
    if( !init_LDS1000_done )
    {
      std::string response;
      
      try
      {
        // Set boolean to avoid write_read to enter again in this funcion
        init_LDS1000_done =true;
        
        // set to remote mode
        response = write_read("mr",false);
        DEBUG_STREAM << "LDS1000::init_LDS1000(): command mr sent "<<  endl;
        
        // active display
        response = write_read("re",false);
        DEBUG_STREAM << "LDS1000::init_LDS1000(): command re sent "<<  endl;
        
      }
      
      catch(Tango::DevFailed &df)
      {
        ERROR_STREAM << df << std::endl;
        
        set_state(Tango::FAULT);
        set_status("Cannot Set LDS 1000 in remote mode ");
        delete mySerial;
        mySerial=0;
        init_LDS1000_done =false;
        
        Tango::Except::re_throw_exception(df,
          (const char*) "CONNECTION_ERROR",
          (const char*) "Cannot send or read data on the SerialLine device ",
          (const char*) "DS1000::init_LDS1000()");
      }
      
    }//endif
    
  }
  // **************************************************************************
  //						average
  //
  //		
  //		argin : array of points
  //		argout: average (double)
  //
  //
  // **************************************************************************
  double LDS1000::average(double * tab)
  {
    int i;
    double mean=0;
    long N,N_0=0;
    
    N = *attr_numberSamples_read;
    
    N_0 = number_of_zero(tab);
    
    for (i=0; i<=N-1; i++)
      
      mean = mean + tab[i];
    
    mean = mean / (N-N_0);
    
    return mean;
    
  }
  
  // **************************************************************************
  //						mean_std_deviation
  //
  //		
  //		argin : array of points (double *)
  //		argout: mean_std_deviation (double)
  //
  //
  // **************************************************************************
  double LDS1000::mean_std_deviation(double * tab)
  {
    double e=0,m;
    int i;
    long N,N_0=0;
    
    N = *attr_numberSamples_read;
    N_0= number_of_zero(tab);
    
    m = average(tab);
    
    for (i=0; i<=N-1; i++)
      
      e = e + (tab[i] - m) * (tab[i] - m);
    
    e = e - (N_0*(m*m));
    
    e = sqrt(e / N-N_0);
    
    return e;
    
  }
  
  // **************************************************************************
  //						number_of_zero
  //
  //    description : looking for the number of "0" in the array 
  //  	
  //		double * : array of points
  //		argout: number of elements "0"
  //
  //
  // **************************************************************************
  long LDS1000::number_of_zero(double * tab)
  {
    
    long i,N,N_0=0;
    
    N = *attr_numberSamples_read;
    
    for (i=0; i<=N-1; i++)
    {
      if(tab[i]==0)
        
        N_0++;
    }
    return N_0;
    
  }
  
  
}	//	namespace
